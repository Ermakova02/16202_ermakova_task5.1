import ru.nsu.ccfit.ermakova.tetris.Tetris;

import javax.swing.*;

public class TetrisApp {
    public static void main(String args[]) throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
    /*    UIManager.setLookAndFeel(UIManager.getCrossPlatformLookAndFeelClassName());
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);*/
        SwingUtilities.invokeLater(new Runnable() {  //создать фрейм в потоке диспетчеризации событий
            @Override
            public void run() {
                new Tetris();
            }
        });
    }
}
