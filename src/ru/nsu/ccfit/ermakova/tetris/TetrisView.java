package ru.nsu.ccfit.ermakova.tetris;

import javax.swing.*;
import java.awt.*;

public class TetrisView extends JPanel { //представление в модели MVC
    private Tetris mvc = null;
    public TetrisView(Tetris mvc) {
        super();
        setLayout(new BorderLayout());
        setFocusable(true);
        this.mvc = mvc;
    }
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        int i, j, x, y, width, height;
        // Draw border
        g.setColor(Color.GRAY);
        int xOffset = (getWidth() - TetrisModel.GROUND_WIDTH) / 2;
        int yOffset = (getHeight() - TetrisModel.GROUND_HEIGHT) / 2;
        for (i = 0; i < TetrisModel.GROUND_BORDER_WIDTH; i++) {
            x = i;
            y = i;
            width = TetrisModel.GROUND_WIDTH - i * 2;
            height = TetrisModel.GROUND_HEIGHT - i * 2;
            g.drawRect(x + xOffset, y + yOffset, width, height);
        }

        // Fill inner part
        g.setColor(Color.WHITE);
        x = TetrisModel.GROUND_BORDER_WIDTH;
        y = TetrisModel.GROUND_BORDER_WIDTH;
        width = TetrisModel.GROUND_WIDTH - TetrisModel.GROUND_BORDER_WIDTH - 1;
        height = TetrisModel.GROUND_HEIGHT - TetrisModel.GROUND_BORDER_WIDTH - 1;
        g.fillRect(x + xOffset, y + yOffset, width, height);

        // Draw cells
        g.setColor(Color.GRAY);
        for (i = 0; i < TetrisModel.GROUND_COLUMNS_NUMBER; i++) {
            x = TetrisModel.GROUND_BORDER_WIDTH + i * TetrisModel.CELL_SIZE - 1;
            y = TetrisModel.GROUND_BORDER_WIDTH - 1;
            width = TetrisModel.GROUND_BORDER_WIDTH + i * TetrisModel.CELL_SIZE - 1;
            height = TetrisModel.GROUND_BORDER_WIDTH + TetrisModel.GROUND_ROWS_NUMBER * TetrisModel.CELL_SIZE - 2;
            g.drawLine(x + xOffset, y + yOffset, width + xOffset, height + yOffset);
        }
        for (i = 0; i < TetrisModel.GROUND_ROWS_NUMBER; i++) {
            x = TetrisModel.GROUND_BORDER_WIDTH - 1;
            y = TetrisModel.GROUND_BORDER_WIDTH + i * TetrisModel.CELL_SIZE - 1;
            width = TetrisModel.GROUND_BORDER_WIDTH + TetrisModel.GROUND_COLUMNS_NUMBER * TetrisModel.CELL_SIZE - 2;
            height = TetrisModel.GROUND_BORDER_WIDTH + i * TetrisModel.CELL_SIZE - 1;
            g.drawLine(x + xOffset, y + yOffset, width + xOffset, height + yOffset);
        }

        // Paint figure
        if (mvc.getModel().getGameStatus() != TetrisModel.GAME_NOT_STARTED)
        {
            Point c = mvc.getModel().getFigure().getCoords();
            x = TetrisModel.GROUND_BORDER_WIDTH + c.x * TetrisModel.CELL_SIZE;
            y = TetrisModel.GROUND_BORDER_WIDTH + c.y * TetrisModel.CELL_SIZE;
            mvc.getModel().getFigure().paint(g, x + xOffset, y + yOffset);
        }

        // Paint lines
        for (i = 0; i < TetrisModel.GROUND_ROWS_NUMBER; i++)
            for (j = 0; j < TetrisModel.GROUND_COLUMNS_NUMBER; j++)
                if (mvc.getModel().getGroundColor(i, j) != 7) {
                    g.setColor(Figure.DEFAULT_COLORS[mvc.getModel().getGroundColor(i, j)]);
                    x = TetrisModel.GROUND_BORDER_WIDTH + j * TetrisModel.CELL_SIZE;
                    y = TetrisModel.GROUND_BORDER_WIDTH + i * TetrisModel.CELL_SIZE;
                    width = TetrisModel.CELL_SIZE - 1;
                    height = TetrisModel.CELL_SIZE - 1;
                    g.fillRect(x + xOffset, y + yOffset, width, height);
                }
    }
}
