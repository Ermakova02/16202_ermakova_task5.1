package ru.nsu.ccfit.ermakova.tetris;

import javax.swing.*;
import java.awt.*;

public class TetrisFrame extends JFrame { //фрейм главного окна
    public static final String MAIN_FRAME_HEAD_TEXT = "Tetris";
    public static final String MAIN_FRAME_HEAD_PAUSED_TEXT = "Tetris (paused)";
    public static final Dimension MAIN_FRAME_SIZE = new Dimension(259, 545);

    public static final String MAIN_FRAME_ICON = "icons/main.png";

    public TetrisFrame() {
        setTitle(MAIN_FRAME_HEAD_TEXT);
        setSize(MAIN_FRAME_SIZE);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource(MAIN_FRAME_ICON)));
        setLocationRelativeTo(null); // Позиционирование по центру экрана
        setResizable(false);
    }
}
