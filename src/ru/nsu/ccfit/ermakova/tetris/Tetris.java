package ru.nsu.ccfit.ermakova.tetris;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.Iterator;
import java.util.Scanner;

public class Tetris implements ActionListener { //основной класс
    private static final String HIGHSCORES_FILENAME = "highscores";
    private static final String DEFAULT_FONT_NAME = "Segoe UI";
    private static final int DEFAULT_FONT_SIZE = 12;

    public static final Dimension BUTTONS_SIZE = new Dimension(91, 26);
    public static final Dimension NEWGAME_DIALOG_SIZE = new Dimension(300, 130);
    public static final Dimension ABOUT_DIALOG_SIZE = new Dimension(300, 100);
    public static final Dimension HIGHSCORES_DIALOG_SIZE = new Dimension(300, 250);
    public static final Dimension GAMEOVER_DIALOG_SIZE = new Dimension(300, 100);

    public static final int SCORES_AREA_ROWS = 10;
    public static final int SCORES_AREA_COLUMNS = 15;
    public static final int TEXT_FIELD_COLUMNS = 20;

    public static final String ABOUT_DIALOG_HEAD = "About Tetris";
    public static final String GAMEOVER_DIALOG_HEAD = "End of game";
    public static final String HIGHSCORES_DIALOG_HEAD = "High Scores";
    public static final String NEWGAME_DIALOG_HEAD = "New Game";

    public static final String ABOUT_DIALOG_LABEL = "Tetris Game. Author: Ekaterina Ermakova.";
    public static final String GAMEOVER_DIALOG_FORMAT_LABEL = "Game over. %s, your score is %d.";
    public static final String NEWGAME_DIALOG_LABEL = "Enter your name:";

    private TetrisFrame frm = null;
    private TetrisController controller = null;
    private TetrisModel model = null;
    private TetrisView view = null;
    private AboutDialog aboutDlg = null;
    private NewGameDialog newgameDlg = null;
    private HighScoresDialog highscoresDlg = null;
    private GameOverDialog gameoverDlg = null;
    private Font defaultFont = null;
    private Scores scores = null;

    public Tetris() {
        initMVC();
        initMenu();
    }

    private void initMVC() {
        scores = new Scores();
        defaultFont = new Font(DEFAULT_FONT_NAME, Font.PLAIN, DEFAULT_FONT_SIZE);
        model = new TetrisModel(this);
        view = new TetrisView(this);
        controller = new TetrisController(this);
        frm = new TetrisFrame();
        frm.add(view, BorderLayout.CENTER);
        frm.getContentPane().add(view);
        frm.setFont(defaultFont);
        frm.setVisible(true);
    }

    private void initMenu() {
        JMenuBar jmb = new JMenuBar();
        JMenu jmMenu = new JMenu("Menu");
        jmMenu.setFont(defaultFont);
        JMenuItem jmiNewGame = new JMenuItem("New Game");
        jmiNewGame.setFont(defaultFont);
        JMenuItem jmiHighScores = new JMenuItem("High Scores");
        jmiHighScores.setFont(defaultFont);
        JMenuItem jmiAbout = new JMenuItem("About");
        jmiAbout.setFont(defaultFont);
        JMenuItem jmiExit = new JMenuItem("Exit");
        jmiExit.setFont(defaultFont);
        jmMenu.add(jmiNewGame);
        jmMenu.add(jmiHighScores);
        jmMenu.add(jmiAbout);
        jmMenu.addSeparator();
        jmMenu.add(jmiExit);

        jmb.add(jmMenu);

        jmiNewGame.addActionListener(this);
        jmiHighScores.addActionListener(this);
        jmiAbout.addActionListener(this);
        jmiExit.addActionListener(this);

        frm.setJMenuBar(jmb);
    }

    private void readScores() {
        boolean fileFound = true;
        Scanner in = null;
        scores.clear();
        try {
            in = new Scanner(new FileReader(HIGHSCORES_FILENAME));
        } catch (FileNotFoundException e) {
//            e.printStackTrace();
            fileFound = false;
        }
        if (!fileFound) return;
        String line = null;
        while (in.hasNext()) {
            line = in.nextLine();
            int index = line.indexOf(";");
            String strScore = line.substring(0, index);
            String name = line.substring(index + 1);
            Integer score = Integer.valueOf(strScore);
            scores.add(new StringIntCont(name, score));
        }
        in.close();
    }
    private void writeScores() {
        FileWriter writer = null;
        try {
            writer = new FileWriter(HIGHSCORES_FILENAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Iterator<StringIntCont> iterator = scores.iterator();
        while (iterator.hasNext()) {
            StringIntCont elem = iterator.next();
            String line = elem.getValue() + ";" + elem.getStr() + "\n";
            try {
                writer.write(line, 0, line.length());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        try {
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateScores(String gamer, int score)
    {
        scores.add(new StringIntCont(gamer, score));
        writeScores();
    }

    public void updateScoresArea(JTextArea scoresArea) {
        readScores();
        Iterator<StringIntCont> iterator = scores.iterator();
        scoresArea.setText(null);
        while (iterator.hasNext()) {
            StringIntCont elem = iterator.next();
            scoresArea.append(elem.getStr() + " " + elem.getValue() + "\n");
        }
    }
    public TetrisController getController() {
        return controller;
    }

    public TetrisModel getModel() {
        return model;
    }

    public TetrisView getView() {
        return view;
    }

    public TetrisFrame getFrame() {
        return frm;
    }

    public void GameOverDialog() {
        model.setGameStatus(TetrisModel.GAME_OVER);
        if(gameoverDlg == null) gameoverDlg = new GameOverDialog(frm, this);
        gameoverDlg.setResults();;
        gameoverDlg.setVisible(true);
    }

    public Font getDefaultFont() { return defaultFont; }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comStr = e.getActionCommand();
        switch (comStr) {
            case "About":
                model.pauseGame();
                if(aboutDlg == null) aboutDlg = new AboutDialog(frm, this);
                aboutDlg.setVisible(true);
                break;
            case "New Game":
                model.pauseGame();
                if(newgameDlg == null) newgameDlg = new NewGameDialog(frm, this);
                newgameDlg.setVisible(true);
                break;
            case "High Scores":
                model.pauseGame();
                if(highscoresDlg == null) highscoresDlg = new HighScoresDialog(frm, this);
                else highscoresDlg.updateScores();
                highscoresDlg.setVisible(true);
                break;
            case "Exit":
                frm.dispose();
                frm = null;
                System.exit(0);
                break;
        }
    }
}
