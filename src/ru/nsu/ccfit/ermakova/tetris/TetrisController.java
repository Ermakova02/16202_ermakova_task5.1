package ru.nsu.ccfit.ermakova.tetris;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class TetrisController implements KeyListener { //контроллер в модели MVC
    private Tetris mvc = null;

    public TetrisController(Tetris mvc) {
        this.mvc = mvc;
        mvc.getView().addKeyListener(this);
    }

    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_UP: mvc.getModel().keyUp(); break;
            case KeyEvent.VK_LEFT: mvc.getModel().keyLeft(); break;
            case KeyEvent.VK_RIGHT: mvc.getModel().keyRight(); break;
            case KeyEvent.VK_SPACE: mvc.getModel().keySpace(); break;
            case KeyEvent.VK_ESCAPE: mvc.getModel().keyEscape(); break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
