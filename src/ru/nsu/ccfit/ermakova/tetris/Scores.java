package ru.nsu.ccfit.ermakova.tetris;

import java.util.*;

public class Scores implements Iterable { //хранит рекорды
    private StringIntComporator scoresComporator;
    private TreeSet<StringIntCont> scoresSet;//для сортировки
    private Map<String, Integer> scoresMap;

    Scores() {
        scoresComporator = new StringIntComporator();
        scoresSet = new TreeSet<StringIntCont>(scoresComporator);
        scoresMap = new HashMap<String, Integer>();
    }

    @Override
    public Iterator iterator() {
        Iterator<StringIntCont> it = new Iterator<StringIntCont>() {
            Iterator<StringIntCont> it = scoresSet.iterator();
            @Override
            public boolean hasNext() { return it.hasNext(); }
            @Override
            public StringIntCont next() { return it.next(); }
            @Override
            public void remove() { throw new UnsupportedOperationException(); }
        };
        return it;
    }

    public void add(StringIntCont score) {
        String newStr = score.getStr();
        Integer newValue = score.getValue();
        if (newValue == 0) return; // Don't save zero results
        if (scoresMap.containsKey(newStr)) {
            Integer existingValue = scoresMap.get(newStr);
            if (newValue > existingValue) {
                scoresSet.remove(new StringIntCont(newStr, existingValue));
                scoresSet.add(score);
                scoresMap.put(newStr, newValue);
            }
        }
        else {
            scoresSet.add(score);
            scoresMap.put(newStr, newValue);
        }
    }

    public void clear() {
        scoresMap.clear();
        scoresSet.clear();
    }
}
