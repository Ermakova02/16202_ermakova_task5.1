package ru.nsu.ccfit.ermakova.tetris;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class NewGameDialog extends JDialog {
    private JTextField nameTextField = null;
    private Tetris mvc = null;
    public NewGameDialog(JFrame owner, Tetris mvc)
    {
        super(owner, Tetris.NEWGAME_DIALOG_HEAD, true);
        this.mvc = mvc;

        JLabel nameLabel = new JLabel(Tetris.NEWGAME_DIALOG_LABEL);
        nameLabel.setFont(mvc.getDefaultFont());
        nameTextField = new JTextField(Tetris.TEXT_FIELD_COLUMNS);
        JPanel fieldsPanel = new JPanel();
        fieldsPanel.add(nameLabel, BorderLayout.WEST);
        fieldsPanel.add(nameTextField, BorderLayout.EAST);
        add(fieldsPanel, BorderLayout.CENTER);

        JButton startBtn = new JButton("Start");
        JButton cancelBtn = new JButton("Cancel");
        startBtn.setPreferredSize(Tetris.BUTTONS_SIZE);
        cancelBtn.setPreferredSize(Tetris.BUTTONS_SIZE);
        JPanel buttonsPanel = new JPanel();
        buttonsPanel.add(startBtn);
        buttonsPanel.add(cancelBtn);
        add(buttonsPanel, BorderLayout.SOUTH);

        nameTextField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }
            @Override
            public void keyPressed(KeyEvent e) {
            }
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) startNewGame();
            }
        });

        startBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                startNewGame();
            }
        });

        startBtn.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }
            @Override
            public void keyPressed(KeyEvent e) {
            }
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) startNewGame();
            }
        });

        cancelBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cancelNewGame();
            }
        });

        cancelBtn.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }
            @Override
            public void keyPressed(KeyEvent e) {
            }
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) cancelNewGame();
            }
        });

        setSize(Tetris.NEWGAME_DIALOG_SIZE);
        setLocationRelativeTo(owner);
//        pack();
        setResizable(false);
    }

    private void startNewGame() {
        if (nameTextField.getText().length() == 0) return;
        setVisible(false);
        mvc.getModel().startNewGame(nameTextField.getText());
    }

    private void cancelNewGame() {
        setVisible(false);
        mvc.getModel().resumeGame();
    }
}
