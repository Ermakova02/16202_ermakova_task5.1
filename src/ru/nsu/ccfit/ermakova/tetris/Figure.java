package ru.nsu.ccfit.ermakova.tetris;

import java.awt.*;
import java.util.Random;

public class Figure {
    public static final int FIGURE_TYPES_NUMBER = 7;
    public static final Color DEFAULT_COLORS[] = {Color.ORANGE, Color.RED, Color.YELLOW, Color.CYAN, Color.PINK, Color.GREEN, Color.MAGENTA, Color.WHITE};
    private static final int ORIENTATION_NUMBERS[] = {1, 2, 2, 2, 4, 4, 4};
    private static final Point DIMENSIONS_INFO[][] = {  {new Point(2, 2)},
                                                        {new Point(4, 1), new Point(1, 4)},
                                                        {new Point(2, 3), new Point(3, 2)},
                                                        {new Point(2, 3), new Point(3, 2)},
                                                        {new Point(2, 3), new Point(3, 2), new Point(2, 3), new Point(3, 2)},
                                                        {new Point(2, 3), new Point(3, 2), new Point(2, 3), new Point(3, 2)},
                                                        {new Point(3, 2), new Point(2, 3), new Point(3, 2), new Point(2, 3)}};
    private static final Point FILLED_SPACE[][][] = {   {{new Point(0, 0), new Point(1, 0), new Point(0, 1), new Point(1, 1)}}, // type 0 orientation 0
                                                        {{new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(3, 0)},  // type 1 orientation 0
                                                         {new Point(0, 0), new Point(0, 1), new Point(0, 2), new Point(0, 3)}}, // type 1 orientation 1
                                                        {{new Point(0, 0), new Point(0, 1), new Point(1, 1), new Point(1, 2)},  // type 2 orientation 0
                                                         {new Point(1, 0), new Point(2, 0), new Point(0, 1), new Point(1, 1)}}, // type 2 orientation 1
                                                        {{new Point(1, 0), new Point(0, 1), new Point(1, 1), new Point(0, 2)},  // type 3 orientation 0
                                                         {new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(2, 1)}}, // type 3 orientation 1
                                                        {{new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(1, 2)}, // type 4 orientation 0
                                                         {new Point(2, 0), new Point(0, 1), new Point(1, 1), new Point(2, 1)}, // type 4 orientation 1
                                                         {new Point(0, 0), new Point(0, 1), new Point(0, 2), new Point(1, 2)}, // type 4 orientation 2
                                                         {new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(0, 1)}}, // type 4 orientation 3
                                                        {{new Point(0, 0), new Point(1, 0), new Point(0, 1), new Point(0, 2)}, // type 5 orientation 0
                                                         {new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(2, 1)}, // type 5 orientation 1
                                                         {new Point(1, 0), new Point(1, 1), new Point(0, 2), new Point(1, 2)}, // type 5 orientation 2
                                                         {new Point(0, 0), new Point(0, 1), new Point(1, 1), new Point(2, 1)}}, // type 5 orientation 3
                                                        {{new Point(1, 0), new Point(0, 1), new Point(1, 1), new Point(2, 1)}, // type 6 orientation 0
                                                         {new Point(0, 0), new Point(0, 1), new Point(1, 1), new Point(0, 2)}, // type 6 orientation 1
                                                         {new Point(0, 0), new Point(1, 0), new Point(2, 0), new Point(1, 1)}, // type 6 orientation 2
                                                         {new Point(1, 0), new Point(0, 1), new Point(1, 1), new Point(1, 2)}}}; // type 6 orientation 3

    private int figType;
    private int figOrient;
    private Point figCoords;
    private int colorNum;
    private boolean invisible;

    Figure() {
        Random rnd = new Random();
        figType = rnd.nextInt(FIGURE_TYPES_NUMBER);
        figOrient = rnd.nextInt(ORIENTATION_NUMBERS[figType]);
        colorNum = figType;
        invisible = false;
        figCoords = new Point(0, 0);
    }

    Figure(Figure another) {
        figType = another.figType;
        figOrient = another.figOrient;
        figCoords = new Point(another.figCoords);
        colorNum = another.getColorNum();
    }

    public Point getCoords() {
        return figCoords;
    }

    public void setCoords(Point coord_arg) {
        figCoords = coord_arg;
    }

    public Point getDimension() {
        Point dim = new Point(DIMENSIONS_INFO[figType][figOrient]);
        return dim;
    }

    public Point[] getFilledSpace() {
        return FILLED_SPACE[figType][figOrient];
    }

    public int getColorNum() { return colorNum; }

    public void rotate() {
        figOrient++;;
        if (figOrient >= ORIENTATION_NUMBERS[figType]) figOrient = 0;
    }

    public void moveLeft() {
        figCoords.x--;
    }

    public void moveRight() {
        figCoords.x++;
    }

    public void moveDown() {
        figCoords.y++;
    }

    public void hide() {
        invisible = true;
    }

    public void unhide() {
        invisible = false;
    }

    public void paint(Graphics g, int offsetX, int offsetY) {
        if (invisible) return;
        g.setColor(DEFAULT_COLORS[colorNum]);
        for (int i = 0; i < 4; i++) {
            int x = offsetX + FILLED_SPACE[figType][figOrient][i].x * TetrisModel.CELL_SIZE;
            int y = offsetY + FILLED_SPACE[figType][figOrient][i].y * TetrisModel.CELL_SIZE;
            int width = TetrisModel.CELL_SIZE - 1;
            int height = TetrisModel.CELL_SIZE - 1;
            g.fillRect(x, y, width, height);
        }
    }
}
