package ru.nsu.ccfit.ermakova.tetris;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class AboutDialog extends JDialog { //О программе
    private Tetris mvc = null;
    public AboutDialog(JFrame owner, Tetris mvc)
    {
        super(owner, Tetris.ABOUT_DIALOG_HEAD, true);
        this.mvc = mvc;

        JLabel textLabel = new JLabel(Tetris.ABOUT_DIALOG_LABEL);
        textLabel.setFont(mvc.getDefaultFont());
        JPanel textPanel = new JPanel();
        textPanel.add(textLabel);
        add(textPanel, BorderLayout.WEST);

        JButton okButton = new JButton("OK");
        okButton.setPreferredSize(Tetris.BUTTONS_SIZE);
        JPanel panel = new JPanel();
        panel.add(okButton);
        add(panel, BorderLayout.SOUTH);

        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeDialog();
            }
        });
        okButton.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }
            @Override
            public void keyPressed(KeyEvent e) {
            }
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) closeDialog();
            }
        });

        setSize(Tetris.ABOUT_DIALOG_SIZE);
        setLocationRelativeTo(owner);
        setResizable(false);
    }

    private void closeDialog() {
        setVisible(false);
        mvc.getModel().resumeGame();
    }
}
