package ru.nsu.ccfit.ermakova.tetris;

import java.util.TimerTask;

public class TetrisTick extends TimerTask { //класс для отсчета времени
    private TetrisModel model = null;

    void setModel(TetrisModel model) { this.model = model; }

    @Override
    public void run() {
        model.nextTick();
    }
}
