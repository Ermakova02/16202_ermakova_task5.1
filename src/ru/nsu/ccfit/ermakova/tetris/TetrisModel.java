package ru.nsu.ccfit.ermakova.tetris;

import java.awt.*;
import java.util.Timer;

public class TetrisModel { //модель в модели MVC
    public static final int GROUND_ROWS_NUMBER = 20;
    public static final int GROUND_COLUMNS_NUMBER = 10;
    public static final int GROUND_WIDTH = 242;
    public static final int GROUND_HEIGHT = 482;
    public static final int GROUND_BORDER_WIDTH = 2;
    public static final int CELL_SIZE = 24;

    public static final int GAME_NOT_STARTED = 0;
    public static final int GAME_IN_PROCESS = 1;
    public static final int GAME_PAUSED = 2;
    public static final int FIGURE_FALLING_DOWN = 3;
    public static final int STICK_FIGURE = 4;
    public static final int GAME_OVER = 5;

    private static final int LINES_REMOVE_DELAY = 300;
    private static final int FALL_DOWN_DELAY = 50;
    private static final int INITIAL_MOVE_DELAY = 500;
    private static final int FIGURES_NUM_IN_ROUND = 10;
    private static final double SPEED_CHANGE_IN_ROUND = 2;

    private static final int BACKGROUND_COLOR_NUM = 7;

    private int ground[][];
    private Figure activeFigure;
    private int gameStatus;
    private int prevGameStatus;
    private String gamer;
    private int score;
    private TetrisTick tickTask;
    private Timer timer;
    private int figuresRoundLeft;
    private int moveDelay;
    private Tetris mvc = null;

    public TetrisModel(Tetris mvc) {
        figuresRoundLeft = FIGURES_NUM_IN_ROUND;
        moveDelay = INITIAL_MOVE_DELAY;
        this.mvc = mvc;
        ground = new int[GROUND_ROWS_NUMBER][GROUND_COLUMNS_NUMBER];
        gameStatus = GAME_NOT_STARTED;
        prevGameStatus = GAME_NOT_STARTED;
        activeFigure = null;
        gamer = null;
        timer = null;
        tickTask = null;
        score = 0;
        clearGround();
    }

    private void clearGround() {
        for (int i = 0; i < GROUND_ROWS_NUMBER; i++)
            for (int j = 0; j < GROUND_COLUMNS_NUMBER; j++)
                ground[i][j] = BACKGROUND_COLOR_NUM;
    }

    public String getGamer() { return gamer; }
    public int getScore() { return score; }
    public void setGameStatus(int status) { gameStatus = status; }

    private boolean checkIntersection(Figure fig) {
        Point coords = fig.getCoords();
        Point dim = fig.getDimension();
        if (coords.y + dim.y > GROUND_ROWS_NUMBER) return true;
        Point space[] = fig.getFilledSpace();
        for (int i = 0; i < 4; i++) {
            Point p = space[i];
            if (ground[coords.y + p.y][coords.x + p.x] != BACKGROUND_COLOR_NUM) return true;
        }
        return false;
    }
    public int getGroundColor(int i, int j) {
        return ground[i][j];
    }

    public int getGameStatus() {
        return gameStatus;
    }

    public Figure getFigure() {
        return activeFigure;
    }

    public void startNewGame(String gamer) {
        figuresRoundLeft = FIGURES_NUM_IN_ROUND;
        moveDelay = INITIAL_MOVE_DELAY;
        this.gamer = gamer;
        clearGround();
        activeFigure = new Figure();
        activeFigure.setCoords(new Point(GROUND_COLUMNS_NUMBER / 2 - 1, 0));
        gameStatus = GAME_IN_PROCESS;
        mvc.getView().updateUI();
        if (timer != null) timer.cancel();
        timer = new Timer(true);
        tickTask = new TetrisTick();
        tickTask.setModel(this);
        timer.schedule(tickTask, moveDelay, moveDelay);
        score = 0;
    }

    public void stickFigure() {
        figuresRoundLeft--;
        if (figuresRoundLeft == 0) moveDelay = (int)((double)moveDelay / SPEED_CHANGE_IN_ROUND);
        if (timer != null) timer.cancel();
        Point space[] = activeFigure.getFilledSpace();
        Point coords = activeFigure.getCoords();
        int c = activeFigure.getColorNum();
        for (int i = 0; i < 4; i++) {
            Point p = space[i];
            ground[coords.y + p.y][coords.x + p.x] = c;
        }
        gameStatus = STICK_FIGURE;
        activeFigure.hide();
        mvc.getView().updateUI();
        timer = new Timer(true);
        tickTask = new TetrisTick();
        tickTask.setModel(this);
        timer.schedule(tickTask, LINES_REMOVE_DELAY, LINES_REMOVE_DELAY);
    }

    public void releaseNewFigure() {
        if (timer != null) timer.cancel();
        activeFigure = new Figure();
        activeFigure.setCoords(new Point(GROUND_COLUMNS_NUMBER / 2 - 1, 0));
        if (!checkIntersection(activeFigure)) {
            gameStatus = GAME_IN_PROCESS;
            mvc.getView().updateUI();
            if (timer != null) timer.cancel();
            timer = new Timer(true);
            tickTask = new TetrisTick();
            tickTask.setModel(this);
            timer.schedule(tickTask, moveDelay, moveDelay);
        }
        else {
            mvc.updateScores(gamer, score);
            mvc.GameOverDialog();
        }
    }

    public boolean removeLine() {
        int i = 0, j = 0, x = 0, y = 0;
        for (i = 0; i < GROUND_ROWS_NUMBER; i++) {
            for (j = 0; j < GROUND_COLUMNS_NUMBER; j++)
                if (ground[i][j] == BACKGROUND_COLOR_NUM) break;
            if (j == GROUND_COLUMNS_NUMBER) break;
        }
        if (i == GROUND_ROWS_NUMBER) return false;
        if (j == GROUND_COLUMNS_NUMBER) {
            score++;
            for (y = i; y > 0; y--)
                for (x = 0; x < GROUND_COLUMNS_NUMBER; x++)
                    ground[y][x] = ground[y - 1][x];
            for (x = 0; x < GROUND_COLUMNS_NUMBER; x++)
                ground[0][x] = BACKGROUND_COLOR_NUM;
            mvc.getView().updateUI();
            return true;
        }
        return false;
    }

    public void keyUp() {
        if (gameStatus == GAME_IN_PROCESS) {
            Figure rotatedFigure = new Figure(activeFigure);
            rotatedFigure.rotate();
            Point dim = rotatedFigure.getDimension();
            Point coords = rotatedFigure.getCoords();
            if (dim.x + coords.x <= GROUND_COLUMNS_NUMBER && !checkIntersection(rotatedFigure)) {
                activeFigure.rotate();
                mvc.getView().updateUI();
            }
        }
    }

    public void keyLeft() {
        if (gameStatus == GAME_IN_PROCESS) {
            Figure movedFigure = new Figure(activeFigure);
            movedFigure.moveLeft();
            Point coords = movedFigure.getCoords();
            if (coords.x >= 0 && !checkIntersection(movedFigure)) {
                activeFigure.moveLeft();
                mvc.getView().updateUI();
            }
        }
    }

    public void keyRight() {
        if (gameStatus == GAME_IN_PROCESS) {
            Figure movedFigure = new Figure(activeFigure);
            movedFigure.moveRight();
            Point dim = movedFigure.getDimension();
            Point coords = movedFigure.getCoords();
            if (coords.x + dim.x  <= GROUND_COLUMNS_NUMBER && !checkIntersection(movedFigure)) {
                activeFigure.moveRight();
                mvc.getView().updateUI();
            }
        }
    }

    public void keySpace() {
        if (gameStatus == GAME_IN_PROCESS) {
            if (timer != null) timer.cancel();
            gameStatus = FIGURE_FALLING_DOWN;
            timer = new Timer(true);
            tickTask = new TetrisTick();
            tickTask.setModel(this);
            timer.schedule(tickTask, FALL_DOWN_DELAY, FALL_DOWN_DELAY);
        }
    }

    public void pauseGame() {
        if (gameStatus == GAME_IN_PROCESS || gameStatus == FIGURE_FALLING_DOWN || gameStatus == STICK_FIGURE) {
            prevGameStatus = gameStatus;
            mvc.getFrame().setTitle(TetrisFrame.MAIN_FRAME_HEAD_PAUSED_TEXT);
            gameStatus = GAME_PAUSED;
        }
    }

    public void resumeGame() {
        if (gameStatus == GAME_PAUSED) {
            mvc.getFrame().setTitle(TetrisFrame.MAIN_FRAME_HEAD_TEXT);
            gameStatus = prevGameStatus;
        }
    }

    public void keyEscape() {
        if (gameStatus == GAME_IN_PROCESS || gameStatus == FIGURE_FALLING_DOWN || gameStatus == STICK_FIGURE) pauseGame();
        else if (gameStatus == GAME_PAUSED) resumeGame();
    }

    public void nextTick() {
        Figure movedFigure = null;
        switch(gameStatus) {
            case GAME_IN_PROCESS:
                movedFigure = new Figure(activeFigure);
                movedFigure.moveDown();
                if (!checkIntersection(movedFigure)) {
                    activeFigure.moveDown();
                    mvc.getView().updateUI();
                }
                else stickFigure();
                break;
            case STICK_FIGURE:
                if (!removeLine()) releaseNewFigure();
                break;
            case FIGURE_FALLING_DOWN:
                movedFigure = new Figure(activeFigure);
                movedFigure.moveDown();
                if (!checkIntersection(movedFigure)) {
                    activeFigure.moveDown();
                    mvc.getView().updateUI();
                }
                else stickFigure();
                break;
        }
    }

    private void dumpGround(String str) {
        int i, j;
        System.out.println(str + ":");
        for (i = 0; i < GROUND_ROWS_NUMBER; i++) {
            for (j = 0; j < GROUND_COLUMNS_NUMBER; j++) {
                System.out.print(ground[i][j] + " ");
            }
            System.out.println();
        }
    }
}
