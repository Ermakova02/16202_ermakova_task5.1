package ru.nsu.ccfit.ermakova.tetris;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class HighScoresDialog extends JDialog {
    private Tetris mvc = null;
    private JTextArea scoresArea;

    public HighScoresDialog(JFrame owner, Tetris mvc) {
        super(owner, Tetris.HIGHSCORES_DIALOG_HEAD, true);
        this.mvc = mvc;

        scoresArea = new JTextArea(Tetris.SCORES_AREA_ROWS, Tetris.SCORES_AREA_COLUMNS);
        scoresArea.setEditable(false);
        JPanel scoresPanel = new JPanel();
        scoresPanel.add(new JScrollPane(scoresArea));
        add(scoresPanel, BorderLayout.CENTER);
        updateScores();

        JButton okButton = new JButton("OK");
        okButton.setPreferredSize(Tetris.BUTTONS_SIZE);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        add(buttonPanel, BorderLayout.SOUTH);

        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setVisible(false);
                mvc.getModel().resumeGame();
            }
        });
        okButton.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }
            @Override
            public void keyPressed(KeyEvent e) {
            }
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) closeDialog();
            }
        });

        setSize(Tetris.HIGHSCORES_DIALOG_SIZE);
        setLocationRelativeTo(owner);
        setResizable(false);
    }

    public void updateScores() {
        mvc.updateScoresArea(scoresArea);
    }

    private void closeDialog() {
        setVisible(false);
        mvc.getModel().resumeGame();
    }
}

