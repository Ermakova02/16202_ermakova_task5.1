package ru.nsu.ccfit.ermakova.tetris;

public class StringIntCont { //используется для рекордов
    private String str;
    private Integer val;
    public StringIntCont() {
        val = 0;
        str = "";
    }
    public StringIntCont(String str, Integer val) {
        this.str = str;
        this.val = val;
    }
    public Integer getValue() { return val; }
    public String getStr() { return str; }
}
