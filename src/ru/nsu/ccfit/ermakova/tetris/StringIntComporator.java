package ru.nsu.ccfit.ermakova.tetris;

import java.util.Comparator;

public class StringIntComporator implements Comparator<StringIntCont> { //класс для сортировки рекордов
    @Override
    public int compare(StringIntCont o1, StringIntCont o2) {
        if (o2.getValue() > o1.getValue()) return 1; // Сортировка в обратном порядке
        if (o2.getValue() < o1.getValue()) return -1; // Сортировка в обратном порядке
        return o1.getStr().compareTo(o2.getStr());
    }
}
