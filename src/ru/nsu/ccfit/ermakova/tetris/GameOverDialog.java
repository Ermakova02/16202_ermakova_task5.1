package ru.nsu.ccfit.ermakova.tetris;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class GameOverDialog extends JDialog {
    private Tetris mvc;
    private JLabel label;

    public GameOverDialog(JFrame owner, Tetris mvc)
    {
        super(owner, Tetris.GAMEOVER_DIALOG_HEAD, true);
        this.mvc = mvc;

        String labelStr = String.format(Tetris.GAMEOVER_DIALOG_FORMAT_LABEL, mvc.getModel().getGamer(), mvc.getModel().getScore());
        label = new JLabel(labelStr);
        label.setFont(mvc.getDefaultFont());
        JPanel labelPanel = new JPanel();
        labelPanel.add(label);
        add(labelPanel, BorderLayout.WEST);

        JButton okButton = new JButton("OK");
        okButton.setPreferredSize(Tetris.BUTTONS_SIZE);
        JPanel buttonPanel = new JPanel();
        buttonPanel.add(okButton);
        add(buttonPanel, BorderLayout.SOUTH);

        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeDialog();
            }
        });
        okButton.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
            }
            @Override
            public void keyPressed(KeyEvent e) {
            }
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) closeDialog();
            }
        });

        setSize(new Dimension(Tetris.GAMEOVER_DIALOG_SIZE));
        setLocationRelativeTo(owner);
        setResizable(false);
    }

    public void setResults() {
        String labelStr = String.format(Tetris.GAMEOVER_DIALOG_FORMAT_LABEL, mvc.getModel().getGamer(), mvc.getModel().getScore());
        label.setText(labelStr);
    }

    private void closeDialog() {
        setVisible(false);
        mvc.getModel().resumeGame();
    }
}
